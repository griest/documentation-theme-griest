# griest's documentation.js theme

A documentation.js theme based on documentation-theme-iam.

The contents are the following:

* `index._`, the main template that defines the document structure
* `section._`, a partial used to render each chunk of documentation
* `assets/*`, any assets, including CSS & JS
